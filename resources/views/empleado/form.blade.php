
<h1>{{ $modo }} Empleado </h1>

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">
        <ul>

            @foreach($errors->all() as $error)

            <li>{{ $error }}</li>

            @endforeach

        </ul>
    </div>

@endif


<div class="form-group">
<label for="Nombre"> Nombre </label>
<input type="text" class="form-control" name="Nombre"  value="{{ isset($empleado->Nombre)?$empleado->Nombre:old('Nombre') }}" id="Nombre" placeholder="Nombre">
</div>

<div class="form-group">
<label for="Nombre"> Apellido Paterno </label>
<input type="text" class="form-control" name="ApellidoPaterno" value="{{ isset($empleado->ApellidoPaterno)?$empleado->ApellidoPaterno:old('ApellidoPaterno') }}"  id="ApellidoPaterno" placeholder="Apellido Paterno">
</div>

<div class="form-group">
<label for="Nombre"> Apellido Materno </label>
<input type="text" class="form-control" name="ApellidoMaterno" value="{{ isset($empleado->ApellidoMaterno)?$empleado->ApellidoMaterno:old('ApellidoMaterno') }}" id="ApellidoMaterno" placeholder="Apellido Materno">
</div>

<div class="form-group">
<label for="Nombre"> Correo </label>
<input type="text" class="form-control" name="Correo" value="{{ isset($empleado->Correo)?$empleado->Correo:old('Correo') }}" id="Correo" placeholder="Correo">
</div>

<input class="btn btn-primary" type="submit" value="{{ $modo }} datos">
<a class="btn btn-success" href="{{ url('empleado/') }}">regresar</a>
