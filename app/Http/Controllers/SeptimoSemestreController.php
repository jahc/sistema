<?php

namespace App\Http\Controllers;

use App\Models\SeptimoSemestre;
use Illuminate\Http\Request;

class SeptimoSemestreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SeptimoSemestre  $septimoSemestre
     * @return \Illuminate\Http\Response
     */
    public function show(SeptimoSemestre $septimoSemestre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SeptimoSemestre  $septimoSemestre
     * @return \Illuminate\Http\Response
     */
    public function edit(SeptimoSemestre $septimoSemestre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SeptimoSemestre  $septimoSemestre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SeptimoSemestre $septimoSemestre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SeptimoSemestre  $septimoSemestre
     * @return \Illuminate\Http\Response
     */
    public function destroy(SeptimoSemestre $septimoSemestre)
    {
        //
    }
}
