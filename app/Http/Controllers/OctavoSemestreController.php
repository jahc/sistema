<?php

namespace App\Http\Controllers;

use App\Models\OctavoSemestre;
use Illuminate\Http\Request;

class OctavoSemestreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OctavoSemestre  $octavoSemestre
     * @return \Illuminate\Http\Response
     */
    public function show(OctavoSemestre $octavoSemestre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OctavoSemestre  $octavoSemestre
     * @return \Illuminate\Http\Response
     */
    public function edit(OctavoSemestre $octavoSemestre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OctavoSemestre  $octavoSemestre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OctavoSemestre $octavoSemestre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OctavoSemestre  $octavoSemestre
     * @return \Illuminate\Http\Response
     */
    public function destroy(OctavoSemestre $octavoSemestre)
    {
        //
    }
}
