<?php

namespace App\Http\Controllers;

use App\Models\QuintoSemestre;
use Illuminate\Http\Request;

class QuintoSemestreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuintoSemestre  $quintoSemestre
     * @return \Illuminate\Http\Response
     */
    public function show(QuintoSemestre $quintoSemestre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuintoSemestre  $quintoSemestre
     * @return \Illuminate\Http\Response
     */
    public function edit(QuintoSemestre $quintoSemestre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuintoSemestre  $quintoSemestre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuintoSemestre $quintoSemestre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuintoSemestre  $quintoSemestre
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuintoSemestre $quintoSemestre)
    {
        //
    }
}
