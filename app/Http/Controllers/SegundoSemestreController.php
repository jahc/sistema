<?php

namespace App\Http\Controllers;

use App\Models\SegundoSemestre;
use Illuminate\Http\Request;

class SegundoSemestreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SegundoSemestre  $segundoSemestre
     * @return \Illuminate\Http\Response
     */
    public function show(SegundoSemestre $segundoSemestre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SegundoSemestre  $segundoSemestre
     * @return \Illuminate\Http\Response
     */
    public function edit(SegundoSemestre $segundoSemestre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SegundoSemestre  $segundoSemestre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SegundoSemestre $segundoSemestre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SegundoSemestre  $segundoSemestre
     * @return \Illuminate\Http\Response
     */
    public function destroy(SegundoSemestre $segundoSemestre)
    {
        //
    }
}
