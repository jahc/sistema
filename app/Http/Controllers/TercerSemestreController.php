<?php

namespace App\Http\Controllers;

use App\Models\TercerSemestre;
use Illuminate\Http\Request;

class TercerSemestreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TercerSemestre  $tercerSemestre
     * @return \Illuminate\Http\Response
     */
    public function show(TercerSemestre $tercerSemestre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TercerSemestre  $tercerSemestre
     * @return \Illuminate\Http\Response
     */
    public function edit(TercerSemestre $tercerSemestre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TercerSemestre  $tercerSemestre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TercerSemestre $tercerSemestre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TercerSemestre  $tercerSemestre
     * @return \Illuminate\Http\Response
     */
    public function destroy(TercerSemestre $tercerSemestre)
    {
        //
    }
}
