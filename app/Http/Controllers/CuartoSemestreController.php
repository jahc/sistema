<?php

namespace App\Http\Controllers;

use App\Models\CuartoSemestre;
use Illuminate\Http\Request;

class CuartoSemestreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CuartoSemestre  $cuartoSemestre
     * @return \Illuminate\Http\Response
     */
    public function show(CuartoSemestre $cuartoSemestre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CuartoSemestre  $cuartoSemestre
     * @return \Illuminate\Http\Response
     */
    public function edit(CuartoSemestre $cuartoSemestre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CuartoSemestre  $cuartoSemestre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CuartoSemestre $cuartoSemestre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CuartoSemestre  $cuartoSemestre
     * @return \Illuminate\Http\Response
     */
    public function destroy(CuartoSemestre $cuartoSemestre)
    {
        //
    }
}
